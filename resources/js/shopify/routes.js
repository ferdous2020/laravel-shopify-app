import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./views/Home";
import OrderList from "./views/OrderList";
import CategoriesList from "./views/CetegoriesList";
import ProductDetails from "./views/ProductDetails";
import CustomerList from "./views/CustomerList";
import CreateCustomer from "./views/CreateCustomer";
import CreateProduct from "./views/CreateProduct";


Vue.use(VueRouter)

const routes = [
    {path: '/', component: Home, name: 'all-product'},
    {path: '/product-details/:id', component: ProductDetails, name: 'product'},
    {path: '/order-list', component: OrderList, name: 'order'},
    {path: '/categories-list', component: CategoriesList, name: 'category'},
    {path: '/customer-list', component: CustomerList, name: 'customers'},
    {path: '/create-customer', component: CreateCustomer, name: 'create-customers'},
    {path: '/create-product', component: CreateProduct, name: 'create-product'},

];

const router = new VueRouter({
    mode: 'history',
    base: process.env.APP_URL,
    routes,
    scrollBehavior() {
        return {x: 0, y: 0};
    }
});


export default router;
