<?php

namespace App\Http\Controllers;

use App\Helpers\ShopifyUrl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ShopifyController extends Controller
{

    /**
     * Get all product from shopify using base url
     * @return \Illuminate\Http\Client\Response
     */
    public function products()
    {
        return Http::get((new ShopifyUrl())->baseURL() . "/admin/api/2020-04/products.json");
    }

    /**
     * Get selected product from shopify by product id
     *
     * @param $id
     * @return \Illuminate\Http\Client\Response
     */
    public function product($id)
    {
        return Http::get((new ShopifyUrl())->baseURL() . "/admin/api/2020-04/products/{$id}.json");
    }
    /**
     * Get selected product from shopify by product id
     *
     * @param $id
     * @return \Illuminate\Http\Client\Response
     */
    public function createCustomer(Request $request)
    {
        return Http::post((new ShopifyUrl())->baseURL() . "/admin/api/2020-04/customers.json",$request->all());
    }
    /**
     * Get selected product from shopify by product id
     *
     * @param $id
     * @return \Illuminate\Http\Client\Response
     */
    public function customers()
    {
        return Http::get((new ShopifyUrl())->baseURL() . "/admin/api/2020-04/customers.json");
    }


}

