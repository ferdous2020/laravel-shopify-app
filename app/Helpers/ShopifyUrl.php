<?php


namespace App\Helpers;


class ShopifyUrl
{
    public function baseURL()
    {
        $api = config('services.shopify.key');
        $secret = config('services.shopify.secret');
        $store = config('services.shopify.store_url');
        return "https://{$api}:{$secret}@{$store}/";
    }
}
