<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// api routes
Route::prefix('api')->group(function () {
    // get all product
    Route::get('/all-product', 'ShopifyController@products');
    // get product by product id
    Route::get('/product/{id}', 'ShopifyController@product');
    // create customer
    Route::get('/customers', 'ShopifyController@customers');
    // create customer
    Route::post('/create-customer', 'ShopifyController@createCustomer');
});

// auth route
Auth::routes();
// home route
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/red', 'ShopifyController@red');
Route::get('/auth', 'ShopifyController@auth');

// load shopify.main blade in base route
Route::get('/{any}', function () {
    return view('shopify.main');
})->where('any', '.*');


